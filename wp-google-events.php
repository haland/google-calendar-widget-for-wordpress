<?php

	/*

		Plugin Name: WP Google Events

		Plugin URI: http://chrishaland.no/wp-google-calendar

		Description: Lets you list the next three events that occur within a year from your Google Calendar

		Version: 0.0.2

		Author: Chris Haland

		Author URI: http://chrishaland.no

	*/ 

	define('WP_PLUGIN_NAME', 'WP Google Events');
	set_include_path(ABSPATH . 'wp-content/plugins/wp-google-calendar/lib/ZendGdata/1.12.10/library');

	require_once('Zend/Loader.php');

	Zend_Loader::loadClass('Zend_Gdata');
	Zend_Loader::loadClass('Zend_Gdata_AuthSub');
	Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
	Zend_Loader::loadClass('Zend_Gdata_Calendar');
	Zend_Loader::loadClass('Zend_Gdata_App_Extension_Link');

	class wp_google_events extends WP_Widget {

		function wp_google_events() {

			$widget_ops = array('description' => __('Lets you list the next upcoming events from your Google Calendar', WP_PLUGIN_NAME));

			parent::WP_Widget(false, __('WP Google Events', WP_PLUGIN_NAME), $widget_ops);

		}

		function widget($args, $instance) {

			extract($args, EXTR_SKIP);

			$link = ($instance['link']) ? apply_filters('widget_link', $instance['link']) : '';

			$eventlimit	= ($instance['eventlimit']) ? apply_filters('widget_eventlimit', $instance['eventlimit']) : 3;

			$calendarlink = ($instance['calendarlink']) ? apply_filters('widget_calendarlink', $instance['calendarlink']) : '';

			$eventless = ($instance['eventless']) ? apply_filters('widget_eventless', $instance['eventless']) : 'Ingen kommende hendelser.';

			$title = ($instance['title']) ? apply_filters('widget_title', $instance['title']) : __('Google Calendar Events', WP_PLUGIN_NAME);

			$pop_width = ($instance['pop_width']) ? apply_filters('widget_pop_width', $instance['pop_width']) : 748;

			$pop_height  = ($instance['pop_height']) ? apply_filters('widget_pop_height', $instance['pop_height']) : 480;


			echo $before_widget; ?>

				<ul> <?php

					echo $before_title;

					if ($link != '') ?>

						<a href="<?php echo bloginfo('url')  . '/' . $link ?>" style="text-decoration:none; color:inherit"> <?php 

							echo $title;

					if ($link != '') ?>

						</a> <?php

					echo $after_title;

				$events = $this->query_events($calendarlink);

				if ($events) {

					$counter = 1;

					foreach ($events as $event) {

						if ($counter <= $eventlimit) {

							$counter++; 

							$url = '';

							foreach ($event->link as $link) {

								if ($link->type == 'text/html') {
									$url = $link->href;
								}

							} 
							
							$url .= (strpos($url, '?') === false) ? '?' : '&';
							$url .= 'ctz=Europe/Oslo';
							
							?>

							<li style="font-weight:normal" onmouseover="this.style.cursor='pointer'" 
								onclick="window.open(<?php echo "'". $url . "'"?>,'_blank',<?php echo "'width=" . $pop_width . ",height=" . $pop_height . "'"?>)"> <?php

								foreach ($event->when as $when) {
									$str_arr = explode('+', $when->startTime);
									$time = strtotime($str_arr[0]);
									$timestamp = date('d/m H:i', $time);

									echo $timestamp . ' ';

									break;

								} 									

								echo $event->title; ?>

							</li> <?php

						} else break;

					}

				} else { ?>

					<li>

						<p> <?php						

							echo __($eventless, WP_PLUGIN_NAME); ?>

						</p>

					</li> <?php

				} ?>

				</ul> <?php

			echo $after_widget;

		}

		function form($instance){

			$instance = wp_parse_args((array) $instance, array('eventlimit' => 3, 'title' => __('Google Calendar Events', WP_PLUGIN_NAME), 'calendarlink' => '', 
				'link' => 'kalender', 'eventless' => 'Ingen kommende hendelser.', 'pop_width' => 748, 'pop_height' => 480));

			$calendarlink = $instance['calendarlink'];

			$eventlimit = $instance['eventlimit'];

			$title = $instance['title'];

			$link = $instance['link']; 

			$eventless = $instance['eventless'];

			$pop_width = $instance['pop_width'];

			$pop_height = $instance['pop_height']; ?>				

			<p>

				<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" class="widefat" value="<?php echo $title; ?>">

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('eventless'); ?>"><?php _e('Text to show when there are no upcoming events', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('eventless'); ?>" name="<?php echo $this->get_field_name('eventless'); ?>" class="widefat" value="<?php echo $eventless; ?>">

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('eventlimit'); ?>"><?php _e('Maximum events displayed', WP_PLUGIN_NAME); ?></label>

				<select id="<?php echo $this->get_field_id('eventlimit'); ?>" name="<?php echo $this->get_field_name('eventlimit'); ?>" class="widefat" style="width:100%;"> <?php

					$limitrange = range(2, 12);

					foreach ($limitrange as $event) {

						$selected_eventlimit = ($event == $eventlimit) ? ' selected="selected"' : '';

						echo '<option value="' . $event . '"' . $selected_eventlimit . '>' . $event . '</option>';

					} ?>

				</select>

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('pop_width'); ?>"><?php _e('Width of event window', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('pop_width'); ?>" name="<?php echo $this->get_field_name('pop_width'); ?>" class="widefat" value="<?php echo $pop_width; ?>">

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('pop_height'); ?>"><?php _e('Height of event window', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('pop_height'); ?>" name="<?php echo $this->get_field_name('pop_height'); ?>" class="widefat" value="<?php echo $pop_height; ?>">

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('link'); ?>"><?php _e('Under category to displaying calendar', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('link'); ?>" name="<?php echo $this->get_field_name('link'); ?>" class="widefat" value="<?php echo $link; ?>">

			</p>

			<p>

				<label for="<?php echo $this->get_field_id('calendarlink'); ?>"><?php _e('Google Calendar Link', WP_PLUGIN_NAME); ?></label>

				<input id="<?php echo $this->get_field_id('calendarlink'); ?>" name="<?php echo $this->get_field_name('calendarlink'); ?>" class="widefat" value="<?php echo $calendarlink; ?>">

			</p> <?php

		}

		function query_events($link) {

			list($h, $u, $w, $c, $f, $user, $calendar, $type) = explode('/', $link);

			$gdataCal = new Zend_Gdata_Calendar();

			$query = $gdataCal->newEventQuery();

			$query->setUser($user);

			$query->setVisibility(($calendar != 'private') ? $calendar : $calendar . '-magicCookie');

			$query->setProjection('full');

			$query->setFutureEvents('true');

			$query->setSingleEvents('true');

			$query->setOrderby('starttime');

			$query->setSortOrder('ascending');

			$query->setMaxResults('14');

			$results = $gdataCal->getCalendarEventFeed($query);						

			if ($results !== false) 

				return $results;

		}

	}

	add_action('widgets_init', create_function('', 'return register_widget("wp_google_events");'));

?>