<?php
	/*
		Plugin Name: WP Google Calendar
		Plugin URI: http://chrishaland.com/wp-google-calendar
		Description: Shows a full calendar of your google calendar events.
		Version: 0.0.2
		Author: Chris Haland
		Author URI: http://chrishaland.com
	*/ 
		
	define('CALENDAR_SHORT_CODE', 'wp_calendar');
	define('WP_PLUGIN_NAME', 'WP Google Calendar');
	define('CALENDAR_SIDEBAR_NAME', 'Calendar Widget');
	
	class wp_google_calendar extends WP_Widget {
		
		function wp_google_calendar() {
			$widget_ops = array('description' => __('Shows a full calendar of your google calendar events.', WP_PLUGIN_NAME));
			parent::WP_Widget(false, __('WP Google Calendar', WP_PLUGIN_NAME), $widget_ops);
		}
			
		function widget($args, $instance){
			extract($args, EXTR_SKIP);
			$calendarlink	= ($instance['calendarlink']) ? apply_filters('widget_calendarlink', $instance['calendarlink']) : '';
			$pop_width = ($instance['pop_width']) ? apply_filters('widget_pop_width', $instance['pop_width']) : 748;
			$pop_height  = ($instance['pop_height']) ? apply_filters('widget_pop_height', $instance['pop_height']) : 480;
				
			echo $before_widget; ?>
				<div id='loading' style='display:none'>Laster...</div>
				<div id='calendar'></div> 
				
				<script type='text/javascript'>
					$(document).ready(function() {					
						$('#calendar').fullCalendar({
		                        editable: true,
		                        selectable: true,
			
		                        header: {
									left: 'prev,next today',
									center: 'title',
									right: 'month,agendaWeek,agendaDay'
								},
								
								events: <?php echo "'" . $calendarlink . "'"?>,
															
								eventClick: function(event) {
									window.open(event.url + (event.url.indexOf('?') == -1 ? '?' : '&') + 'ctz=Europe/Oslo', 'gcalevent', <?php echo "'width=" . $pop_width . 
										",height=" . $pop_height . "'"?>);
									return false;
								},
						
								loading: function(bool) {
									if (bool) {
										$('#loading').show();
									}else{
										$('#loading').hide();
									}
								}
						
							});	
						});	
				</script> <?php
	
			echo $after_widget;
		}
			
		function form($instance){
			$instance = wp_parse_args((array) $instance, array('calendarlink' => '', 'pop_width' => 748, 'pop_height' => 480));
			$calendarlink = $instance['calendarlink'];
			$pop_width = $instance['pop_width'];
			$pop_height = $instance['pop_height']; ?>
			
			<p>
				<label><?php _e('Shortcode:', WP_PLUGIN_NAME); ?></label>
				<label><?php _e('['. CALENDAR_SHORT_CODE .']', WP_PLUGIN_NAME); ?></label>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('calendarlink'); ?>"><?php _e('Google Calendar Link', WP_PLUGIN_NAME); ?></label>
				<input id="<?php echo $this->get_field_id('calendarlink'); ?>" name="<?php echo $this->get_field_name('calendarlink'); ?>" class="widefat" value="<?php echo $calendarlink; ?>">
			</p> 
			<p>
				<label for="<?php echo $this->get_field_id('pop_width'); ?>"><?php _e('Width of event window', WP_PLUGIN_NAME); ?></label>
				<input id="<?php echo $this->get_field_id('pop_width'); ?>" name="<?php echo $this->get_field_name('pop_width'); ?>" class="widefat" value="<?php echo $pop_width; ?>">
			</p>
			<p>
				<label for="<?php echo $this->get_field_id('pop_height'); ?>"><?php _e('Height of event window', WP_PLUGIN_NAME); ?></label>
				<input id="<?php echo $this->get_field_id('pop_height'); ?>" name="<?php echo $this->get_field_name('pop_height'); ?>" class="widefat" value="<?php echo $pop_height; ?>">
			</p> <?php
		}
	}
	
	add_action('widgets_init', create_function('', 'return register_widget("wp_google_calendar");'));
	
	function calendar_shortcode($atts) {
		register_calendar_sidebar();

		ob_start();
		
		if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar(CALENDAR_SIDEBAR_NAME) ) : endif;
		$sidebar = ob_get_contents();
		
		ob_end_clean();
		
		return $sidebar;
	}
	
	add_shortcode(CALENDAR_SHORT_CODE, 'calendar_shortcode'); 
	
	function register_calendar_sidebar() {
		register_sidebar(array(
					'name'					=> CALENDAR_SIDEBAR_NAME,
					'id' 						=> 'calendar-widget',
					'description'   => __( 'Located on a page.'),
					'before_widget' => '<div id="%2$s" class="widget">',
					'after_widget' => '</div>',
					'before_title' => '<p><strong>',
					'after_title' => '</p><strong>',
				));
	}
	
	add_action('admin_init', 'register_calendar_sidebar'); 
	
	function register_used_scripts() { ?>
		<link rel='stylesheet' type='text/css' href="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/fullcalendar.css' ?>">
		<link rel='stylesheet' type='text/css' href="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/fullcalendar.print.css' ?>" media='print'>
			
		<style type='text/css'>
			#loading {
				position: absolute;
				top: 5px;
				right: 5px;
			}
			
			#calendar {
				width: 900px;
				margin: 0 auto;
				font: 100%/1.35em Arial, Helvetica, sans-serif;
			}	
		</style>
			
		<!-- If another plugin uses another version of jquery, this might cause problems.
				<script type='text/javascript' src="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/lib/jquery.min.js' ?>"></script> 
		-->
		<script type='text/javascript' src="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/lib/jquery-ui.custom.min.js' ?>"></script>
		<script type='text/javascript' src="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/fullcalendar.min.js' ?>"></script>
		<script type='text/javascript' src="<?php echo bloginfo('url') . '/wp-content/plugins/wp-google-calendar/lib/fullcalendar/2.2.6/gcal.js' ?>"></script> <?php
	}
	
	add_action('wp_head', 'register_used_scripts');
?>